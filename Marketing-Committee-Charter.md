# Marketing Committee Interim Charter

##

## Objective

The purpose of this document is to establish an interim Grant Approval procedure for the Marketing Committee to allow grant proposals to be processed using a streamlined approach between now and the full launch of the Accumulate mainnet. The interim procedure will act as a stepping stone toward the official, community-sanctioned Grant Approval procedure.

## Marketing definition

Grant proposals that fall under the purview of the Marketing Committee include all applications, tools, solutions, and services that provide marketing gain to the Accumulate protocol.

- **Marketing Programs:** any program including but not limited to distribution, public relations, media planning and/or buying, branding, and social media, that has defined and measurable goals.
- **Community Growth:** any initiative that grows the Accumulate community on its various channels: Twitter, Telegram, YouTube, Discord, Reddit, and any others.
- **Exchange Promotions:** any campaign co-led by an exchange that encourages users of the exchange to use ACME or earn ACME.
- **Ecosystem Marketing:** any co-marketing with ecosystem projects that have mutually beneficial growth for Accumulate and the ecosystem project. For example, De Facto proposed a co-marketing campaign for the Accumulate Bridge that would benefit both projects.
- **Marketing Tools:** any program/application that can automate aspects of the promotion of the Accumulate protocol.
- **Incentives:** any incentivization structure that could get small amounts of ACME to users to increase usage of the protocol.

All applications paid for by an Accumulate grant should be developed under an open-source license.

## Initial Committee Members

The initial interim committee will be made up of three members.

Confirmed initial committee members:

- Jay Singh, Chairperson
- Kristine Thomassen
- Drew Mailen
- TJ
- Anastasia Javurek

## Committee Chairperson

The committee chairperson is responsible for tracking and documenting all proposals, and ensuring timely decision-making by the committee. The committee chairperson should document all proposals and their current status in a spreadsheet that is accessible by all committee members. The committee chairperson is also responsible for scheduling meetings of the committee as needed.

## Grant Types

This committee will be responsible for reviewing and approving or denying all grant types for ecosystem products that will be paid for with ACME in the Accumulate grant pool.

- Accumulate Open Calls / Requests for Proposal (RFP)
- Grant Applications from the community
- Fast-track Grant Requests

Please review [Accumulate Grant System](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md#three-types-of-grant-programs) for more information

## Grant Submission

Grant proposals are to be submitted using a Google Form (or similar service) that will be hosted on the Accumulate website. RFPs can be broadcast through the Accumulate social media channels and website, but should link to the same Google Form.

The proposal form should include:

- Applicant contact information (including emai)
- Summary of the marketing program
- The measurable metrics of the program
- Requested ACME amount
- Description of use of funds
- Multimedia material (presentations, videos, etc)
- Portfolio or links to other projects

If further information is needed before a decision can be made, the committee can request a Demo from the applicant.

## Grant Payment

Budget: 25% of grant pool – 15M ACME (Please review the Budget section of [Accumulate Grant System](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md#budgets) for more information)

Because the ACME allocated towards the Ecosystem grant budget will not be available until mainnet launch, the only payment agreement option available for the Ecosystem Committee is an agreement for future tokens. Payment can also be distributed in installments, to ensure progress.

## Committee Organization

Communication between committee members can be done both synchronously and asynchronously. A Discord channel in the Accumulate Discord will be set up for committee members to discuss proposals that have been received. This channel can be made publicly viewable, but will only be writeable by committee members. Exceptions can be made for Accumulate stakeholders whose opinion is needed. Meetings can be scheduled ad hoc if synchronous discussion is needed.

In addition, the Ecosystem committee should have a routinely scheduled Review and Retrospective meeting (starting on a monthly basis and adjusted as needed). This meeting should be used to review the state of all products that have been approved grants and overall performance of the grantee development groups, as well to review the performance of the committee itself.

## Decisions

Committee members must come to a decision on a grant within two weeks of a proposal being received. The committee must come to one of the following decisions:

- **Approved** : The grant proposal has been approved.
- **Tentative Approval:** The grant proposal may be approved if certain changes are made to the proposal and then resubmitted (e.g. grant amount or scope of project).
- **Rejected:** The grant proposal will be declined and cannot be resubmitted.
- **Decision Postponed:** The grant proposal cannot be approved or denied at the current time, but should be taken up at a later date within a reasonable timeframe.

If a Grant Proposal is approved, the applicant should be required to submit a git repository link for the product. The applicant should also be required to submit a monthly status report.

## Agreement Procedure

Some kind of repeatable services document where the deliverables are variable in an Exhibit. Document stays the same, entity changes,
