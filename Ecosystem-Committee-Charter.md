# Ecosystem Committee Interim Charter

## Objective 

The purpose of this document is to establish an interim Grant Approval procedure for the Ecosystem Committee to allow grant proposals to be processed using a streamlined approach between now and the full launch of the Accumulate mainnet. The interim procedure will act as a stepping stone toward the official, community-sanctioned Grant Approval procedure.

## Ecosystem definition

Grant proposals that fall under the purview of the Ecosystem Committee include all applications, tools, solutions, and services that provide utility to, or derive utility from, the Accumulate core protocol.

- Engagement tools: Tools that allow participants to engage directly with the core protocol.
- Developer tools: Software or services that allow for easier, more efficient, and more secure development on top of the core protocol.
- DApps/Web3: Decentralized Applications that provide a service that derives utility from the core protocol.
- Cross-chain and intra-chain connectivity: Tools that allow users to port tokens from other blockchain networks, or from different chains within a blockchain network with a multi-chain structure.
- Tokens: Includes tokens that are issued as standalone products without a directly corresponding application or service.
- Web2/Other applications: Traditional software that uses the core protocol in a variety of novel ways.
- Educational resources: Resources that help developers or end users learn more about the protocol. Resources can be text, multimedia, or even interactive (software).

All applications paid for by an Accumulate grant should be developed under an open-source license.

## Initial Committee Members

The initial interim committee will be made up of three members. 
Confirmed initial committee members:
- Quentin Vidal (DeFi Devs) – Chairman
- Maarten Boender (Sphereon)
- Kyle Michelson

## Committee Chairman

The committee chairman is responsible for tracking and documenting all proposals, and ensuring timely decision-making by the committee. The committee chairman should document all proposals and their current status in a spreadsheet that is accessible by all committee members. The committee chairman is also responsible for scheduling meetings of the committee as needed.

## Grant Types

This committee will be responsible for reviewing and approving or denying all grant types for ecosystem products that will be paid for with ACME in the Accumulate grant pool. 
- Accumulate Open Calls / Requests for Proposal (RFP)
- Grant Applications from the community
- Fast-track Grant Requests

Please review [Accumulate Grant System](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md#three-types-of-grant-programs) for more information.

## Grant Submission

Grant proposals are to be submitted using a Google Form (or similar service) that will be hosted on the Accumulate website. RFPs can be broadcast through the Accumulate social media channels and website, but should link to the same Google Form.

#### The proposal form should include:

- Applicant contact information (including email and Discord username)
- Summary of the product
- Value product brings the protocol
- Requested ACME amount
- Description of use of funds
- Multimedia material (presentations, videos, etc)
- Github, Gitlab, or Bitbucket account
- Portfolio or links to other projects

If further information is needed before a decision can be made, the committee can request a Demo from the applicant.

## Grant Payment

Budget: 15% of grant pool – 9 million ACME (Please review the [Budgets](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md#budgets) section of Accumulate Grant System for more information)

Because the ACME allocated towards the Ecosystem grant budget will not be available until mainnet launch, the only payment agreement option available for the Ecosystem Committee is an agreement for future tokens. Payment can also be distributed in installments, to ensure progress.

## Committee Organization
Communication between committee members can be done both synchronously and asynchronously. A Discord channel in the Accumulate Discord will be set up for committee members to discuss proposals that have been received. This channel can be made publicly viewable, but will only be writeable by committee members. Exceptions can be made for Accumulate stakeholders whose opinion is needed. Meetings can be scheduled ad hoc if synchronous discussion is needed.

In addition, the Ecosystem committee should have a routinely scheduled Review and Retrospective meeting (starting on a monthly basis and adjusted as needed). This meeting should be used to review the state of all products that have been approved grants and overall performance of the grantee development groups, as well to review the performance of the committee itself.

## Decisions
Committee members must come to a decision on a grant within two weeks of a proposal being received. The committee must come to one of the following decisions:
- Approved: The grant proposal has been approved.
- Tentative Approval: The grant proposal may be approved if certain changes are made to the proposal and then resubmitted (e.g. grant amount or scope of project).
- Rejected: The grant proposal will be declined and cannot be resubmitted.
- Decision Postponed: The grant proposal cannot be approved or denied at the current time, but should be taken up at a later date within a reasonable timeframe.

If a Grant Proposal is approved, the applicant should be required to submit a git repository link for the product. The applicant should also be required to submit a monthly status report.

## Scoring and decision making procedure
Please see [Accumulate Grant System](https://gitlab.com/accumulatenetwork/governance/governance-docs/-/blob/main/Grants.md) for more information on scoring.

You can view the list of submitted grant applications and their decisions [here](https://docs.google.com/spreadsheets/d/1gO3glqcqJDyUcoaosiIkaBJxn07KJIKT4KgX-wAW6Ug/edit?usp=sharing)

